﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeslaModel
{
    public class EFTeslaCarRepository : ITeslacar
    {
        private CarDBContext db = new CarDBContext();
        public override IQueryable<TeslaCar> TeslaCars
        { get { return db.TeslaCars; } }

        public override TeslaCar Save(TeslaCar TeslaCar)
        {
            db.TeslaCars.Add(TeslaCar);
            db.SaveChanges();
            return TeslaCar;
        }

        public override void Delete(int id)
        {
            var car1 = db.TeslaCars.Single(m => m.CarId == id);
            db.TeslaCars.Remove(car1);
            db.SaveChanges();
        }
    }
}