﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeslaModel
{
    public abstract class ITeslacar
    {
        public abstract IQueryable<TeslaCar> TeslaCars { get; }
        public abstract TeslaCar Save(TeslaCar Teslacar);
        public abstract void Delete(int id);
    }
}
