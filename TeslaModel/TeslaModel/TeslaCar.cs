﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TeslaModel
{
    public class TeslaCar
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CarId { get; set; }

        public String CarName { get; set; }
        public int CarPrice { get; set; }
        public int CarMaxSpeed { get; set; }
        public string CarExtras { get; set; }
    }

    public class CarDBContext : DbContext
    {
        public CarDBContext()
        { }
        public DbSet<TeslaCar>  TeslaCars{ get; set; }
    }
}