﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace TeslaModel.Controllers
{
    
    public class HomeController : Controller
    {
        public static SpeechSynthesizer sSynth = new SpeechSynthesizer();
        public static PromptBuilder pBuilder = new PromptBuilder();
        public SpeechRecognitionEngine sRecognize = new SpeechRecognitionEngine();

        private CarDBContext db = new CarDBContext();

        private ITeslacar TeslaRepo;

        public HomeController()
        {
            this.TeslaRepo = new EFTeslaCarRepository();
        }

        public HomeController( ITeslacar TeslaCarRepo)
        {
            this.TeslaRepo = TeslaCarRepo;
        }
        // GET: Home
        public ViewResult Index()
        {
             ViewBag.Message = "Index";
            var car1 = from c in TeslaRepo.TeslaCars
                orderby c.CarId
                select c;
            return View(car1.ToList());
        }

        // GET: Home/Details/5
        public async Task<ActionResult> Details(int id)
        {
            ViewBag.Message = "Details";
            var car1 = db.TeslaCars.SingleOrDefault(e => e.CarId == id);
            pBuilder.ClearContent();
            pBuilder.AppendText("The details are as follows :" + "Car Id is:" + car1.CarId.ToString() + "Car Name is:" + car1.CarName + "Car Price is:" + car1.CarPrice.ToString() + "Car Max Speed is:" + car1.CarMaxSpeed.ToString() + "Car Extras are:" + car1.CarExtras);
            sSynth.SpeakAsync(pBuilder);
            return View(car1);
        }

        // GET: Home/Create
        public ActionResult Find()
        {
            return  View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Find(TeslaCar car1)
        {
            try
            {
                TeslaCar car12 = new TeslaCar();
                car12 = TeslaRepo.TeslaCars.Where(car10 => car10.CarName == car1.CarName).FirstOrDefault();
                if (car12==null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction(
                        "Details",
                        "Home",
                        new {id = car12.CarId}
                    );
                   
                }
            }
            catch
            {
                return View("Delete");
            }
        }
        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(TeslaCar car1)
        {
            try
            {
                TeslaRepo.Save(car1);
              //  db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            var car1 = db.TeslaCars.Single(m => m.CarId == id);
            return View(car1);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var car1 = db.TeslaCars.Single(m => m.CarId == id);
                if (TryUpdateModel(car1))
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(car1);
            }
            catch
            {
                return View();
            }
        }


        // GET: Home/Delete/5
        public ViewResult Delete(int id)
        {
            var car1 = db.TeslaCars.SingleOrDefault(e => e.CarId == id);
            return View(car1);
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                
                TeslaRepo.Delete(id);
               // db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Index");
            }
        }
    }
}
