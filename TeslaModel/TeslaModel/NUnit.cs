﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using System.Web.Mvc;
using Moq;
using TeslaModel.Controllers;

namespace TeslaModel
{
    [TestFixture]
    public class NUnit
    {
        private CarDBContext db = new CarDBContext();

        [Test]
        public void TestIndexAccess()
        {
            var obj = new HomeController();

            var actResult = obj.Index() as ViewResult;
            Assert.IsNotEmpty(actResult.ViewData);
        }

        [Test]
        public void TeslaCarIndexViewContainsList()
        {
            //Arrange
            TeslaCar car12;
            Mock<ITeslacar> mock = new Mock<ITeslacar>();
            mock.Setup(m => m.TeslaCars).Returns(new List<TeslaCar> 
            {
                new TeslaCar { CarId = 12 , CarName = "Tester" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "In" },
                new TeslaCar { CarId = 123 , CarName = "Testing" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "Out" },
                new TeslaCar { CarId = 1234 , CarName = "Testingddf" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "Outa" }

            }.AsQueryable());
            HomeController controller = new HomeController(mock.Object);
            //Act
           var actual =  controller.Index();
            //  var actual = controller.Index();
            
            //Assert
          Assert.AreEqual(((List<TeslaCar>)actual.Model).Count().ToString(),"3");
           
        }

        [Test]
        public void TeslaCarIndexInstanceCheck()
        {
            //Arrange
            TeslaCar car12;
            Mock<ITeslacar> mock = new Mock<ITeslacar>();
            mock.Setup(m => m.TeslaCars).Returns(new List<TeslaCar>
            {
                new TeslaCar { CarId = 12 , CarName = "Tester" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "In" },
                new TeslaCar { CarId = 123 , CarName = "Testing" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "Out" },
                new TeslaCar { CarId = 1234 , CarName = "Testingddf" , CarMaxSpeed = 69 , CarPrice = 6969 , CarExtras = "Outa" }

            }.AsQueryable());
            HomeController controller = new HomeController(mock.Object);

            //Act
            var actual = (List<TeslaCar>) controller.Index().Model;

            //Assert
            Assert.IsInstanceOf<List<TeslaCar>>(actual);
        }

    }
}